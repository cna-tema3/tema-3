﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGraphics
{
    public class User
    {
        public string Name;
        public string message;
        public User()
        {
            this.Name = null;
        }

        public User(string name, string message)
        {
            this.Name = name;
            this.message = message;
        }

        public string getName()
        {
            return Name;
        }

        public void setName(string name)
        {
            this.Name = name;
        }

        public string getMessage()
        {
            return message;
        }

        public void setMessage(string message)
        {
            this.message = message;
        }
    }
}
