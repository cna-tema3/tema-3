﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;

namespace Server
{
    public class ChatRoom
    {
        private ConcurrentDictionary<string, IServerStreamWriter<Message>> users = new ConcurrentDictionary<string, IServerStreamWriter<Message>>();

        public void SendMessage(string name, IServerStreamWriter<Message> response)
        {
            users.TryAdd(name, response);          
            string fileName = "../../Log.txt";
            string fullPath = Path.GetFullPath(fileName);
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(fullPath, true))
            {
                file.WriteLine('[' + DateTime.Now.ToString() + ']');
            }
        }

        public void Remove(string name)
        {
            users.TryRemove(name, out var s);
        }

        public async Task BroadcastMessageAsync(Message message) => await BroadcastMessage(message);

        private async Task BroadcastMessage(Message message)
        {
            if (message != null)
            {
                if (message.Text == message.User + " has joined the room")
                {
                    System.Console.WriteLine('[' + DateTime.Now.ToString() + ']');
                    Console.WriteLine(message.Text);
                }
                if (message.Text == message.User + " has left the room")
                {
                    System.Console.WriteLine('[' + DateTime.Now.ToString() + ']');
                    Console.WriteLine(message.Text);
                }
                string fileName = "../../Log.txt";
                string fullPath = Path.GetFullPath(fileName);
                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fullPath, true))
                {
                    file.WriteLine($"{message.User}: {message.Text}");
                }
            }

            foreach (var user in users.Where(x => x.Key != message.User))
            {
                var item = await SendMessageToSubscriber(user, message);
                if (item != null)
                {
                    Remove(item?.Key);
                }
            }
        }

        private async Task<Nullable<KeyValuePair<string, IServerStreamWriter<Message>>>> SendMessageToSubscriber(KeyValuePair<string, IServerStreamWriter<Message>> user, Message message)
        {
            try
            {
                await user.Value.WriteAsync(message);
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return user;
            }
        }
    }

}
