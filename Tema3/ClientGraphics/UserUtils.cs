﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGraphics
{
    public class UserUtils
    {
        public static User user = new User();

        public static void SetName(string name)
        {
            user.Name = name;
        }
        public static void SetMessage(string message)
        {
            user.message = message;
        }
        public static string GetUser()
        {
            return user.Name;
        }
        public static string GetMessage()
        {
            return user.message;
        }
        public static string TextChat()
        {
            return $"{user.Name}: {user.message}";
        }
        public static void setProperties(string name, string messsage)
        {
            user.Name = name;
            user.message = messsage;
        }
        public static string TextChatProp(string name, string text)
        {
            return $"{name}: {text}";
        }
    }
}
