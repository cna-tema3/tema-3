﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGraphics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {



        const string Host = "localhost";
        const int Port = 16842;
        Channel channel;
        Generated.ChatService.ChatServiceClient client;
        //private AsyncDuplexStreamingCall<Message, Message> chat;

        public List<Message> messageList;
        private string name;

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await ReceiveMessages();
           // channel.ShutdownAsync().Wait();
        }
        

        public MainWindow()
        {
            InitializeComponent();

            channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);
            client = new Generated.ChatService.ChatServiceClient(channel);

            messageList = new List<Message>();

            //chat = client.SendMessage();

            
            //channel.ShutdownAsync().Wait();
        }

        private void AddMessInListView()
        {
            foreach (var mess in messageList)
            {
                ListViewItem item = new ListViewItem();
                item.Content = mess.User + ":" + mess.Text;
                lv_mess.Items.Add(item);
            }
        }

        private async void btnSetName_Click(object sender, RoutedEventArgs e)
        {
            name = txtName.Text;
            using (var chat = client.SendMessage())
            {
                await chat.RequestStream.WriteAsync(new Message { User = name, Text = $"{name} has joined the room" });
            }
            ListViewItem item = new ListViewItem();
            item.Content = $"{name} has joined the room";
            lv_mess.Items.Add(item);
            
            //_ = ReceiveMessages();
        }

        private async void btnSendMessage_Click(object sender, RoutedEventArgs e)
        {
            string mess = txtMess.Text;
            using (var chat = client.SendMessage())
            {
                if (chat != null)
                {
                    await chat.RequestStream.WriteAsync(new Message { User = name, Text = mess });
                    messageList.Add(new Message { User = name, Text = mess });
                    ListViewItem item = new ListViewItem();
                    item.Content = name + ":" + mess;
                    lv_mess.Items.Add(item);
                }
                //await chat.RequestStream.CompleteAsync();
            }
 
            txtMess.Clear();
        }

        private async Task ReceiveMessages()
        {
            using (var chat = client.SendMessage())
            {
                _ = Task.Run(async () =>
                {
                    while (await chat.ResponseStream.MoveNext(cancellationToken: CancellationToken.None))
                    {
                        var response = chat.ResponseStream.Current;
                        messageList.Add(response);
                        AddMessInListView();
                    }                    
                });
               // await chat.RequestStream.CompleteAsync();
            }          
        }
    }
}
