﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    internal class ChatService : Generated.ChatService.ChatServiceBase
    {
        private readonly ChatRoom _chatRoomService;

        public ChatService(ChatRoom chatRoomService)
        {
            _chatRoomService = chatRoomService;
        }

        public override async Task SendMessage(IAsyncStreamReader<Message> requestStream, IServerStreamWriter<Message> responseStream, ServerCallContext context)
        {
            if (!await requestStream.MoveNext()) return;

            do
            {
                _chatRoomService.SendMessage(requestStream.Current.User, responseStream);
                await _chatRoomService.BroadcastMessageAsync(requestStream.Current);
            } while (await requestStream.MoveNext());

            _chatRoomService.Remove(context.Peer);
        }
    }
}
